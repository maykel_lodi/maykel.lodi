package br.com.lodistudio.agencetest.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Banco extends SQLiteOpenHelper {
    private static final String NOME_BANCO = "agence.db";
    private static final int VERSAO = 1;

    public Banco(Context context) {
        super(context, NOME_BANCO, null, VERSAO);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE USUARIO(";
        sql = sql + "ID integer primary key autoincrement,";
        sql = sql + "LOGIN text not null,";
        sql = sql + "SENHA text not null)";
        db.execSQL(sql);

        //Cadastro do usuário padrão
        sql = "INSERT INTO USUARIO (LOGIN, SENHA) VALUES(";
        sql = sql + "'agence',";
        sql = sql + "'agence')";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS USUARIO");
        onCreate(db);
    }
}
