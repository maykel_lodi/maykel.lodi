package br.com.lodistudio.agencetest.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class Controlador {

    private static Controlador controlador;
    private SQLiteDatabase db;

    public Controlador(Context applicationContext) {
        Banco database = new Banco(applicationContext);
        db = database.getReadableDatabase();
    }


    public static Controlador getInstance(Context context) {
        if (controlador == null) {
            controlador = new Controlador(context);
        }
        return controlador;
    }

    public SQLiteDatabase getDatabase(){
        return this.db;
    }


}
