package br.com.lodistudio.agencetest.Activitys;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import br.com.lodistudio.agencetest.Database.Controlador;
import br.com.lodistudio.agencetest.R;

public class Login extends Activity implements View.OnClickListener {
    EditText login, senha;
    Button entrar;
    Controlador bd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        login = (EditText) findViewById(R.id.login_usuario);
        senha = (EditText) findViewById(R.id.login_senha);
        entrar = (Button) findViewById(R.id.login_bt_entrar);

        entrar.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.login_bt_entrar){
            bd = Controlador.getInstance(Login.this);
            Cursor cursor = bd.getDatabase().rawQuery("SELECT * FROM USUARIO WHERE LOGIN = '" + login.getText().toString() + "' AND SENHA = '" + senha.getText().toString() + "'", null);
            if (cursor.moveToFirst()){
                Intent intent = new Intent(Login.this, Main.class);
                startActivity(intent);
            }else{
                Toast.makeText(Login.this, "Usuário e/ou Senha inválidos!", Toast.LENGTH_LONG).show();
            }
            cursor.close();
        }
    }
}
